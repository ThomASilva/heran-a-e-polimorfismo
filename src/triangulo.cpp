#include "triangulo.hpp"
#include <iostream>
#include <math.h>

using namespace std;

Triangulo::Triangulo(){
	set_base(7.0);
	set_altura(4.0);
	set_tipo("Triângulo");
}
Triangulo::Triangulo(string tipo, float base, float altura){
//	cout << "Construtor da Classe Triangulo com sobrecarga\n";
	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}
Triangulo::~Triangulo(){
	cout << "Destrutor da Classe Triangulo\n";
}
float Triangulo::calcula_area(){
	return get_base()*get_altura()/2;
}
float Triangulo::calcula_perimetro(){
	float hip; //hipotenusa² = (base/2)²+(altura)²
	hip = sqrt( ( pow(get_base()/2,2) + pow(get_altura(),2) ) );
	return hip + get_base() + get_altura(); 

}
void Triangulo::imprime(){
	cout << "Tipo: " << get_tipo();
	cout << "\nBase: " << get_base();
	cout << "\nAltura: " << get_altura();
	cout << "\nArea: " << calcula_area();
	cout << "\nPerimetro: " << calcula_perimetro() << endl << endl;
}

