#include <iostream>
#include "formageometrica.hpp"
#include "triangulo.hpp"
#include "quadrado.hpp"
#include "circulo.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"
#include <iomanip>
#include <vector>

using namespace std;

int main (int argc, char * argv[])
{
	cout << fixed << setprecision(2);

	vector<FormaGeometrica *> formas;

	formas.push_back(new Triangulo("Triangulo", 2, 4));
	formas.push_back(new Quadrado("Quadrado", 1.1, 1.1));
	formas.push_back(new Circulo("Circulo", 1.5));
	formas.push_back(new Paralelogramo("Paralelogramo", 2.5, 3.1));
	formas.push_back(new Pentagono("Pentagono", 1));
	formas.push_back(new Hexagono("Hexagono", 2));


	for(FormaGeometrica * f: formas){
		cout << endl << "Tipo: " << f->get_tipo() << endl;
		cout << "Area: " << f->calcula_area() << endl;
		cout << "Perimetro: " << f->calcula_perimetro() << endl;
	}

	putchar('\n');

	
	return 0;
}
