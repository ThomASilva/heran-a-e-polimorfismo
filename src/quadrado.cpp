#include "quadrado.hpp"

Quadrado::Quadrado(){
	set_tipo("Generico");
	set_base(0.0);
	set_altura(0.0);
}
Quadrado::Quadrado(string tipo, float base, float altura){
//	cout << "Construtor da Classe Quadrado com sobrecarga\n";
	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}
Quadrado::~Quadrado(){
	cout << "Destrutor da Classe Quadrado\n";
}
