#include "circulo.hpp"
//#include "formageometrica.hpp"
#include <math.h>
#include <iostream>

using namespace std;

Circulo::Circulo(){
	set_tipo("Circulo");
	set_raio(0.0);
}
Circulo::Circulo(string tipo, float raio){
//	cout << "Construtor da Classe Circulo com sobrecarga\n";
	set_tipo(tipo);
	set_raio(raio);
}
Circulo::~Circulo(){
	cout << "Destrutor da Classe Circulo\n";
}

float Circulo::get_raio(){
	return raio;
}
void Circulo::set_raio(float raio){
	this->raio = raio;
}
float Circulo::calcula_area(){
	return M_PI * pow(get_raio(),2);
}
float Circulo::calcula_perimetro(){
	return 2 * M_PI * get_raio();
}
void Circulo::imprime(){
	cout << "Tipo: " << get_tipo() << endl;
	cout << "Raio: " << get_raio() << endl;
	cout << "Area: " << calcula_area() << endl;
	cout << "Perimetro: " << calcula_perimetro() << endl << endl;
}
