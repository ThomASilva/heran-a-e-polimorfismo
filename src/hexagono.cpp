#include "hexagono.hpp"
#include <iostream>

using namespace std;

Hexagono::Hexagono(){
	cout << "Construtor da Classe Hexano" << endl;
	set_tipo("Generico");
	set_base(0.0);
	set_altura(0.0);
}

Hexagono::Hexagono(string tipo, float base){
//	cout << "Construtor da Classe Hexagono com sobrecarga" << endl;
	set_tipo(tipo);
	set_base(base);
	set_altura((base/2)/0.577);
}
Hexagono::~Hexagono(){
	cout << "Destrutor da Classe Hexagono" << endl;
}
float Hexagono::calcula_area(){
	return (get_base() * get_altura()/2) * 6; //area dos triangulos que formam o hexagono x6
}
float Hexagono::calcula_perimetro(){
	return get_base()*6; //perimetro é a soma de todas as bases dos 6 triangulos do hexagono
}
void Hexagono::imprime(){
	cout << "Tipo: " << get_tipo() << endl;
	cout << "Base: " << get_base() << endl;
	cout << "Altura: " << get_altura() << endl;
	cout << "Perimetro: " << calcula_perimetro() << endl;
	cout << "Area: " << calcula_area() << endl << endl;
}
