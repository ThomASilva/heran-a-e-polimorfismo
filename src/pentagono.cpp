#include "pentagono.hpp"
#include <iostream>

using namespace std;

Pentagono::Pentagono(){
	set_tipo("Generico");
	set_base(0.0);
	set_altura(0.0);
}
Pentagono::Pentagono(string tipo, float base){
//	cout << "Construtor da Classe Pentagono com sobrecarga" << endl;
	set_tipo(tipo);
	set_base(base);
	set_altura((base/2)/0.727);
	//this->altura = ((base/2)/0.727);
}
Pentagono::~Pentagono(){
	cout << "Destrutor da classe Pentagono" << endl;
}
float Pentagono::calcula_perimetro(){
	return get_base() * 5;
}
float Pentagono::calcula_area(){
	//float h;
	//h=(get_base()/2)/0.727; //h = (base/2)/tg 36°

	return calcula_perimetro() * get_altura() / 2; //considerando que o pentagono tem lado = base
//e a altura é dos triangulos internos que formam o pentagono xD
}
void Pentagono::imprime(){
	cout << "Tipo: " << get_tipo() << endl;
	cout << "Base: " << get_base() << endl;
	cout << "Altura: " << get_altura() << endl;
	cout << "Area: " << calcula_area() << endl;
	cout << "Perimetro: " << calcula_perimetro() << endl << endl;
}
