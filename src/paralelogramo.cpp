#include "paralelogramo.hpp"
#include <iostream>

using namespace std;

Paralelogramo::Paralelogramo(){
	set_tipo("Generico");
	set_base(0.0);
	set_altura(0.0);
}
Paralelogramo::Paralelogramo(string tipo, float base, float altura){
//	cout << "Construtor da Classe Paralelogramo com sobrecarga\n";
	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}
Paralelogramo::~Paralelogramo(){
	cout << "Destrutor da Classe Paralelogramo\n";
}
