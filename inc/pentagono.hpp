#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP
#include "formageometrica.hpp"

class Pentagono : public FormaGeometrica{

public:
	Pentagono();
	Pentagono(string tipo, float base);
	~Pentagono();

	float calcula_perimetro();
	float calcula_area();
	void imprime();
};

#endif
