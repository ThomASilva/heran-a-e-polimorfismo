#ifndef QUADRADO_HPP
#define QUADRADO_HPP
#include <iostream>
#include "formageometrica.hpp"

using namespace std;

class Quadrado : public FormaGeometrica{

public:
	Quadrado();
	Quadrado(string tipo, float base, float altura);
	~Quadrado();
};


#endif
