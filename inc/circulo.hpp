#ifndef CIRCULO_HPP
#define CIRCULO_HPP
#include "formageometrica.hpp"
#include <iostream>

class Circulo : public FormaGeometrica{

private:
	float raio;
public:
	Circulo();
	Circulo(string tipo, float raio);
	~Circulo();

	float get_raio();
	void set_raio(float raio);

	float calcula_area();
	float calcula_perimetro();
	void imprime();
};

#endif
